<div align="center">
  <h1>Niqqud</h1>
  <p>
    A lightweight rust library for removing hebrew diacritics (ניקוד).
  </p>
  <h2>

[![CI](https://github.com/benny-n/niqqud/actions/workflows/ci.yml/badge.svg)](https://github.com/benny-n/niqqud/actions/workflows/ci.yml)
![LICENSE](https://img.shields.io/github/license/benny-n/niqqud)
![CRATES](https://img.shields.io/crates/v/niqqud)

</h2>

</p>
